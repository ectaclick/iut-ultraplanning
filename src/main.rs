
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::collections::HashMap;

use std::path::{Path, PathBuf};

use log::{debug, error, info};

use curl::easy::Easy;

use serde::Deserialize;

use icalendar::Calendar;

const DOWNLOADED_ICAL_PATH: &str = "./edt";
const GENERATED_ICAL_PATH: &str = "./generated";
const CONFIG_FILE_PATH: &str = "./config.json";

#[derive(Debug,  Deserialize)]
struct Config {
    pub edt_urls : HashMap<String, String>,
}

fn main()
-> Result<(), Box<dyn std::error::Error>> {

    env_logger::init();

    let mut config_file = File::open(CONFIG_FILE_PATH)
        .expect(&format!("Failed to read file '{}'", CONFIG_FILE_PATH));

    let mut config_file_content = String::new();
    config_file.read_to_string(&mut config_file_content)
        .expect("Failed to read config file content");

    let config: Config = serde_json::from_str(&config_file_content)
        .expect("Failed to parse config file content");

    for (group, url) in config.edt_urls {
        info!("Processing group '{group}, url: {url}'");
        match process_ical(&group, &url){
            Ok(_) => info!("Ical of group {group} procesed"),
            Err(ref e) => error!("{e}"),
        };
    }

    Ok(())
}

fn process_ical(group: &str, url: &str)
-> Result<(), Box<dyn std::error::Error>> {

    download_ical(&group, &url).expect(
        &format!("Failed to dowload ical from '{}'", &url)
    );

    let ical_file_path = Path::new(DOWNLOADED_ICAL_PATH)
        .join(group)
        .with_extension("ics");

    let mut reader = BufReader::new(File::open(&ical_file_path)?);
    info!("File '{}' found", &ical_file_path.display());

    let mut content = String::new();
    reader.read_to_string(&mut content)?;

    let icalendar = icalendar::parser::read_calendar(&content)?;
    info!("File '{}' parsed", &ical_file_path.display());
    debug!("{}", &icalendar);
    debug!("icalendar event count: {}", &icalendar.components.len());

    let mut amphis_ical = Calendar::new();
    let mut autonomies_ical = Calendar::new();
    let mut controles_ical = Calendar::new();
    let mut hollidays_ical = Calendar::new();
    let mut td_tp_ical = Calendar::new();

    let mut event_placed = false;

    for component in icalendar.components {

        for property in &component.properties {
            let property_name = property.name.as_ref().to_lowercase();
            let value = property.val.as_ref().to_lowercase();

            if property_name == "summary" {


                if value.contains("autonom") {
                    autonomies_ical.components.push(component.clone().into());
                    debug!("autonomie | {}", &value);
                    event_placed = true;
                    break;
                }

                if value.contains("vacance")
                || value.contains("férié") {
                    hollidays_ical.components.push(component.clone().into());
                    debug!("vacances | {}", &value);
                    event_placed = true;
                    break;
                }
            } else if property_name == "location" {

                if value.contains("1a02") {
                    controles_ical.components.push(component.clone().into());
                    debug!("controle | {}", &value);
                    event_placed = true;
                    break;
                }

                if value.contains("1a") || value.contains("amphi") {
                    amphis_ical.components.push(component.clone().into());
                    debug!("amphi | {}", &value);
                    event_placed = true;
                    break;
                }
            }
        }

        if !event_placed {
            debug!("TP/TD");
            td_tp_ical.components.push(component.into());
        }
        event_placed = false;
    }

    debug!("amphis event count: {}", amphis_ical.components.len());
    debug!("autnomies event count: {}", autonomies_ical.components.len());
    debug!("controles event count: {}", controles_ical.components.len());
    debug!("holidays event count: {}", hollidays_ical.components.len());
    debug!("TD/TP event count: {}", td_tp_ical.components.len());

    let generated_ical_path = Path::new(&GENERATED_ICAL_PATH)
        .join(group);


    std::fs::create_dir_all(&generated_ical_path)
        .expect(
            &format!("Failed to create '{}' folder", &generated_ical_path.display())
    );

    let _ = export_ical_to_file(generated_ical_path.join("amphis"), &amphis_ical);
    let _ = export_ical_to_file(generated_ical_path.join("autonomies"), &autonomies_ical);
    let _ = export_ical_to_file(generated_ical_path.join("controles"), &controles_ical);
    let _ = export_ical_to_file(generated_ical_path.join("hollidays"), &hollidays_ical);
    let _ = export_ical_to_file(generated_ical_path.join("tp_tp"), &td_tp_ical);

    Ok(())
}

fn download_ical(group: &str, url: &str)
-> Result<(), curl::Error> {

    let file_path = Path::new(DOWNLOADED_ICAL_PATH)
        .join(group)
        .with_extension("ics");

    let mut curl_easy = Easy::new();

    curl_easy.url(&url)
        .expect("Failed to set curl interface");

    std::fs::create_dir_all(&DOWNLOADED_ICAL_PATH)
        .expect(
            &format!("Failed to create EDT directory '{}'", &DOWNLOADED_ICAL_PATH)
        );

    let mut ical_file = File::create(&file_path)
        .expect(
            &format!("Failed to create '{}' file", &file_path.display())
        );

    curl_easy.write_function(move |data| {
        match ical_file.write(data){
            Ok(_) => {},
            Err(e) => error!("Error '{}' when writing in file '{}'",
                e,
                &file_path.display()
            ),
        };

        Ok(data.len())
    }).expect("Failed to write data in edt file");

    curl_easy.perform()
        .expect(
            &format!("Failed to get data from {}", &url)
        );

    Ok(())
}

fn export_ical_to_file(mut file_path: PathBuf, ical: &Calendar)
-> Result<(), std::io::Error> {

    file_path.set_extension("ics");

    std::fs::File::create(format!("{}", &file_path.display()))?
        .write_all(format!("{}", &ical).as_bytes())?;

    info!("Ical generated at {}", &file_path.display());

    Ok(())
}

#[cfg(test)]
mod test {

    const DOWNLOADED_ICAL_PATH: &str = "./edt";

    use std::io::prelude::*;
    use std::io::BufReader;
    use std::fs::File;
    use std::path::{PathBuf, Path};

    use icalendar::Calendar;
    use crate::{download_ical, export_ical_to_file};

    #[test]
    fn test_download_ical()
        -> Result<(), Box<dyn std::error::Error>> {

        dotenvy::dotenv()?;

        let edt_url = std::env::var("EDT_URL").expect("Failed to get ENV variable 'EDT_URL'");

        download_ical("TEST", &edt_url).expect(
            &format!("Failed to dowload ical from '{}'", &edt_url)
        );

        assert!(Path::exists(
            &Path::new(DOWNLOADED_ICAL_PATH).join("TEST.ics").as_path())
        );

        Ok(())
    }

    fn load_ical_from_frile()
    -> Result<Calendar, Box<dyn std::error::Error>> {

        let mut reader = BufReader::new(File::open("./test/edt.ics")?);

        let mut content = String::new();
        reader.read_to_string(&mut content)?;

        Ok(icalendar::parser::read_calendar(&content)?.into())
    }


    #[test]
    fn test_icalendar_parser()
    -> Result<(), Box<dyn std::error::Error>> {

        let icalendar = load_ical_from_frile()?;

        assert_eq!(94, icalendar.components.len());

        Ok(())
    }

    #[test]
    fn test_save_ical()
    -> Result<(), Box<dyn std::error::Error>> {

        let icalendar = load_ical_from_frile()?;

        std::fs::create_dir_all("./generated/test")
            .expect("Failed to create './generated/test' folder");

        let _ = export_ical_to_file(PathBuf::from("./generated/test/icalendar_test"), &icalendar);
        assert!(Path::new("./generated/test/icalendar_test.ics").exists());

        Ok(())
    }

}
